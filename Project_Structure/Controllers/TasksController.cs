﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService service;
        public TasksController(ITaskService taskService)
        {
            service = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetTasks()
        {
            return service.GetAllTasks().ToList();
        }

        [HttpGet("{taskId}")]
        public ActionResult<TaskDTO> GetTaskById(int taskId)
        {
            var task = service.GetTaskById(taskId);

            if (task == null)
            {
                return NotFound();
            }

            return task;
        }
        [HttpPost]
        public ActionResult CreateTask(TaskDTO task)
        {
            service.CreateTask(task);
            return Ok(task);
        }
        [HttpPut]
        public ActionResult UpdateTask(TaskDTO task)
        {
            var existingTask = service.GetTaskById(task.Id);

            if (existingTask == null)
            {
                return NotFound();
            }

            service.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{taskId}")]
        public ActionResult DeleteTask(int taskId)
        {
            var taskToDelete = service.GetTaskById(taskId);

            if (taskToDelete == null)
            {
                return NotFound();
            }

            service.DeleteTask(taskId);
            return NoContent();
        }


        [HttpGet("tasksByUser/{userId}")]
        public ActionResult<IEnumerable<TaskDTO>> TasksByUser(int userId)
        {

            return service.TasksByUser(userId).ToList();
        }
        [HttpGet("finished/{userId}")]
        public ActionResult<IEnumerable<Tuple<int, string>>> FinishedThisYearTasksByUser(int userId)
        {
            return service.FinishedTasksByUser(userId).ToList();
        }
        [HttpGet("unFinished/{userId}")]
        public ActionResult<IEnumerable<TaskDTO>> UnFinishedTasksByUser(int userId)
        {
            return service.UnFinishedTasksByUser(userId).ToList();
        }
    }
}