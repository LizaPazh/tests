﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TaskService: ITaskService
    {
        private readonly IProjectTaskRepository _taskRep;
        private readonly IUserRepository _userRep;
        private readonly IMapper _mapper;
        public TaskService(IProjectTaskRepository rep, IMapper mapper, IUserRepository userRepository)
        {
            _taskRep = rep;
            _mapper = mapper;
            _userRep = userRepository;
        }
        public TaskDTO GetTaskById(int taskId)
        {
            return _mapper.Map<TaskDTO>(_taskRep.GetById(taskId));
        }
        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_taskRep.GetAll());
        }
        public void CreateTask(TaskDTO taskDTO)
        {
            if (taskDTO == null)
            {
                throw new ArgumentNullException();
            }
            var task = _mapper.Map<ProjectTask>(taskDTO);
            _taskRep.Create(task);
        }
        public void UpdateTask(TaskDTO taskDTO)
        {
            var existingTask = _taskRep.GetById(taskDTO.Id);

            if(existingTask == null)
            {
                throw new NotFoundException($"Task with {taskDTO.Id} not found");
            }

            var task = _mapper.Map<ProjectTask>(taskDTO);
            _taskRep.Update(task);
        }
        public void DeleteTask(int taskId)
        {
            _taskRep.Delete(taskId);
        }
        public IEnumerable<TaskDTO> TasksByUser(int userId)
        {
            var user = _userRep.GetById(userId);
            if (user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            return _mapper.Map<IEnumerable<TaskDTO>>(_taskRep.GetAll().Where(t => t.PerformerId == userId && t.Name.Length < 45));
        }
        public IEnumerable<Tuple<int, string>> FinishedTasksByUser(int userId)
        {
            var user = _userRep.GetById(userId);
            if (user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            return _taskRep.GetAll().Where(t => t.PerformerId == userId && t.FinishedAt.Year == DateTime.Now.Year)
                         .Select(c => new Tuple<int, string>(c.Id, c.Name));
        }

        public IEnumerable<TaskDTO> UnFinishedTasksByUser(int userId)
        {
            var user = _userRep.GetById(userId);
            if(user == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
            var tasks =  _taskRep.GetAll().Where(t => t.PerformerId == userId && t.State != TaskState.Finished);
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }
    }
}
