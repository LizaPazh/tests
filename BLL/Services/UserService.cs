﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRep;
        private readonly IProjectRepository _projectRep;
        private readonly IMapper _mapper;
        public UserService(IUserRepository userRep, IProjectRepository projectRep, IMapper mapper)
        {
            _userRep = userRep;
            _projectRep = projectRep;
            _mapper = mapper;
        }
        public UserDTO GetUserById(int userId)
        {
            return _mapper.Map<UserDTO>(_userRep.GetById(userId));
        }
        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_userRep.GetAll());
        }
        public void CreateUser(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException();
            }
            var user = _mapper.Map<User>(userDTO);
            _userRep.Create(user);
        }
        public void UpdateUser(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            _userRep.Update(user);
        }
        public void DeleteUser(int userId)
        {
            _userRep.Delete(userId);
        }
        public IEnumerable<UserDTO> UsersByABCWithTasksSortedByName()
        {
            return _userRep.GetAll(u=>u.ProjectTasks)
                .OrderBy(u => u.FirstName).AsEnumerable()
                .Select(u => { u.ProjectTasks = u.ProjectTasks.OrderByDescending(t => t.Name.Length); return _mapper.Map<UserDTO>(u); });
        }

        public UserWithParameters GetUsersCharacteristics(int userId)
        {
            var existingUser = _userRep.GetById(userId);

            if (existingUser == null)
            {
                throw new NotFoundException($"User with {userId} not found");
            }
           
            var user = _userRep.GetAll(u=>u.ProjectTasks).FirstOrDefault(u=>u.Id == userId);
            var lastProject = _projectRep.GetAll(p=>p.ProjectTasks).Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefault();
         
            return new UserWithParameters
            {
                User = _mapper.Map<UserDTO>(user),
                LastProject = _mapper.Map<ProjectDTO>(lastProject),
                LastProjectTasks = lastProject?.ProjectTasks?.Count() ?? 0,
                NotCompletedOrCanceledTasks = user.ProjectTasks.Where(t => t.State != TaskState.Finished).Count(),
                LongestTask =  _mapper.Map<TaskDTO>(user.ProjectTasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault())
            };
        }
    }
}
