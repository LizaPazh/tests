﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface ITaskService
    {
        TaskDTO GetTaskById(int taskId);
        IEnumerable<TaskDTO> GetAllTasks();
        void CreateTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void DeleteTask(int taskId);
        IEnumerable<TaskDTO> TasksByUser(int userId);

        IEnumerable<Tuple<int, string>> FinishedTasksByUser(int userId);

        IEnumerable<TaskDTO> UnFinishedTasksByUser(int userId);
    }
}
