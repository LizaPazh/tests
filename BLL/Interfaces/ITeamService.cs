﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface ITeamService
    {
        TeamDTO GetTeamById(int teamId);
        IEnumerable<TeamDTO> GetAllTeams();
        void CreateTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void DeleteTeam(int teamId);
        IEnumerable<TeamWithUsers> GetTeamsByUserAge();
    }
}
