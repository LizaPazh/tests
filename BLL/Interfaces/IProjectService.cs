﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IProjectService
    {
        ProjectDTO GetProjectById(int projectId);
        IEnumerable<ProjectDTO> GetAllProjects();
        void CreateProject(ProjectDTO project);
        void UpdateProject(ProjectDTO project);
        void DeleteProject(int projectId);
        Dictionary<ProjectDTO, int> CountTasksForProjectByUser(int userId);
        IEnumerable<ProjectWithParameters> GetProjectWithCharacteristics();
    }
}
