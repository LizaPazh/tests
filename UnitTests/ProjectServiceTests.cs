using AutoMapper;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Models;
using DAL.Repositories;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace UnitTests
{
    public class ProjectServiceTests
    {
        readonly IMapper _mapper;
        readonly List<Project> projects;
        readonly Project project1;
        readonly Project project2;

        public ProjectServiceTests()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });
            _mapper = new Mapper(config);

            project1 = new Project
            {
                Id = 24,
                Name = "Neque quia nam autem iure.",
                Description = "Eligendi voluptatem debitis. Ipsa quod et porro omnis et aut dolores. Ad aut qui sit. Esse amet error.",
                CreatedAt = new DateTime(2020, 07, 01, 01, 48, 27, 78),
                Deadline = new DateTime(2020, 10, 04, 02, 04, 02, 7),
                AuthorId = 1,
                TeamId = 9,
                Team = new Team
                {
                    Id = 1,
                    Name = "debitis",
                    CreatedAt = new DateTime(2020, 07, 01, 13, 59, 44, 16),
                    Users = new List<User>() {
                        new User {
                            Id = 5,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 1,
                            },
                        new User {
                            Id = 16,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 1,
                        },
                        new User {
                            Id = 24,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 1,
                        }
                    }
                },
                ProjectTasks = new List<ProjectTask>()
                {
                    new ProjectTask
                        {
                        Id = 115,
                        Name = "Similique possimus ipsam aliquid maiores laborum optio.",
                        Description = "Nihil sint qui illum et libero. Dolorem illum dolorem optio ipsam. Ea odit quis aut et autem non vel iusto.",
                        CreatedAt = new DateTime(2020,07,01,13,59,44,16),
                        FinishedAt = new DateTime(2020,10,16,14,22,44,65),
                        State = TaskState.Canceled,
                        ProjectId = 24,
                        PerformerId = 3
                        }
                }
            };

            project2 = new Project
            {
                Id = 36,
                Name = "Aut molestiae sequi id expedita.",
                Description = "Atque molestias qui omnis assumenda exercitationem.Impedit deserunt aut.Nihil praesentium eius. Distinctio doloribus excepturi vel.",
                CreatedAt = new DateTime(2020, 07, 01, 01, 48, 27, 78),
                Deadline = new DateTime(2020, 11, 15, 02, 04, 02, 7),
                AuthorId = 1,
                TeamId = 7,
                Team = new Team
                {
                    Id = 2,
                    Name = "debitis",
                    CreatedAt = new DateTime(2020, 07, 01, 13, 59, 44, 16),
                    Users = new List<User>() {
                        new User {
                            Id = 6,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 2,
                            },
                        new User {
                            Id = 17,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 2,
                        },
                        new User {
                            Id = 25,
                            FirstName = "Retha",
                            LastName = "Will",
                            Email = "Retha67@yahoo.com",
                            Birthday = new DateTime(2007,07,01,13,59,44,16),
                            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                            TeamId = 2,
                        }
                    }
                },
                ProjectTasks = new List<ProjectTask>()
                {
                    new ProjectTask
                    {
                        Id = 115,
                        Name = "Similique possimus ipsam aliquid maiores laborum optio.",
                        Description = "Nihil sint qui illum et libero. Dolorem illum dolorem optio ipsam. Ea odit quis aut et autem non vel iusto.",
                        CreatedAt = new DateTime(2020,07,01,13,59,44,16),
                        FinishedAt = new DateTime(2020,10,16,14,22,44,65),
                        State = TaskState.Canceled,
                        ProjectId = 24,
                        PerformerId = 3
                    }
                }
            };
            
            projects = new List<Project>() { project1, project2};
        }

        [Fact]
        public void CountTasksForProjectByUser_WhenUserWithId1_Return2ProjectsTaskCount()
        {
            int userId = 1;
   
            var _projectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => _projectRepository.GetAll(A<Expression<Func<Project, object>>>.Ignored))
                .Returns(projects);

            var service = new ProjectService(_projectRepository, _mapper);

            var result = service.CountTasksForProjectByUser(userId);

            Assert.Equal(projects.Count(), result.Keys.Count);
            Assert.Equal(project1.Id, result.Keys.First().Id);
            Assert.Equal(project2.Id, result.Keys.Last().Id);
            Assert.Equal(project1.ProjectTasks.Count(), result.Values.First());
            Assert.Equal(project2.ProjectTasks.Count(), result.Values.Last());

        }
        [Fact]
        public void GetProjectWithCharacteristics_When2Projects_Return2Projects()
        {

            var _projectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => _projectRepository.GetAll(A<Expression<Func<Project, object>>>.Ignored, A<Expression<Func<Project, object>>>.Ignored))
                .Returns(projects);

            var service = new ProjectService(_projectRepository, _mapper);

            var result = service.GetProjectWithCharacteristics();

            Assert.Equal(projects.Count(), result.Count());
            Assert.Equal(project1.Id, result.First().Project.Id);
            Assert.Equal(project2.Id, result.Last().Project.Id);
        }
    }
}
