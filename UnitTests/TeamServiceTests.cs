﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Models;
using DAL.Repositories;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class TeamServiceTests
    {
        readonly IMapper _mapper;
        readonly List<Team> teams;

        readonly Team team1 = new Team
        {
            Id = 1,
            Name = "debitis",
            Users = new List<User>()
            {
                new User
                {
                    Id = 5,
                    FirstName = "Retha",
                    LastName = "Will",
                    Email = "Retha67@yahoo.com",
                    Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
                    RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                    TeamId = 1,
                },
                new User {
                    Id = 6,
                    FirstName = "Retha",
                    LastName = "Will",
                    Email = "Retha67@yahoo.com",
                    Birthday = new DateTime(2006, 07, 01, 13, 59, 44, 16),
                    RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                    TeamId = 1,
                }
            }
        };

        readonly Team team2 = new Team
        {
            Id = 2,
            Users = new List<User>()
            {
                new User
                {
                    Id = 7,
                    FirstName = "Retha",
                    LastName = "Will",
                    Email = "Retha67@yahoo.com",
                    Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
                    RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                    TeamId = 2,
                },
                new User {
                    Id = 8,
                    FirstName = "Retha",
                    LastName = "Will",
                    Email = "Retha67@yahoo.com",
                    Birthday = new DateTime(2016, 07, 01, 13, 59, 44, 16),
                    RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                    TeamId = 2,
                }
            }
        };

        public TeamServiceTests()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });

            _mapper = new Mapper(config);

            teams = new List<Team>() { team1, team2 };
        }

        [Fact]
        public void GetTeamsByUserAge_WhenTwoTeams_ThenReturnFirstTeam()
        {
            var _teamRepository = A.Fake<ITeamRepository>();
            A.CallTo(() => _teamRepository.GetAll(A<Expression<Func<Team, object>>>.Ignored))
                .Returns(teams);

            var service = new TeamService(_teamRepository, _mapper);

            var result = service.GetTeamsByUserAge();

            Assert.Single(result);
            Assert.Equal(team1.Id, result.First().TeamId);           
        }

        [Fact]
        public void UpdateTeam_WhenAdd1UserToFirstTeam_ThenUpdateIsHappened()
        {
            var updatedTeam = new TeamDTO
            {
                Id = team1.Id,
                Name = "debitis",
                Users = new List<UserDTO>()
                {
                    new UserDTO
                    {
                        Id = 5,
                        FirstName = "Retha",
                        LastName = "Will",
                        Email = "Retha67@yahoo.com",
                        Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
                        RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                        TeamId = 1,
                    },
                    new UserDTO {
                        Id = 6,
                        FirstName = "Retha",
                        LastName = "Will",
                        Email = "Retha67@yahoo.com",
                        Birthday = new DateTime(2006, 07, 01, 13, 59, 44, 16),
                        RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                        TeamId = 1,
                    },
                    new UserDTO {
                        Id = 7,
                        FirstName = "Retha",
                        LastName = "Will",
                        Email = "Retha67@yahoo.com",
                        Birthday = new DateTime(2006, 07, 01, 13, 59, 44, 16),
                        RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                        TeamId = 1,
                    }
                }
            };
            

            var _teamRepository = A.Fake<ITeamRepository>();
            A.CallTo(() => _teamRepository.GetById(team1.Id))
                 .Returns(team1);

            var service = new TeamService(_teamRepository, _mapper);

            service.UpdateTeam(updatedTeam);

            A.CallTo(() => _teamRepository.Update(A<Team>.That.Matches(t => t.Id == updatedTeam.Id && t.Users.Count() == updatedTeam.Users.Count())))
                                          .MustHaveHappenedOnceExactly();

        }

        [Fact]
        public void UpdateTeam_WhenTeamNotExists_ExceptionThrown()
        {
            var notExistedId = 1000;

            var updatedTeam = new TeamDTO
            {
                Id = notExistedId
            };

            var _teamRepository = A.Fake<ITeamRepository>();
            A.CallTo(() => _teamRepository.GetById(notExistedId))
                 .Returns(null);

            var service = new TeamService(_teamRepository, _mapper);

            Assert.Throws<NotFoundException>(() => service.UpdateTeam(updatedTeam));
        }
    }
}
