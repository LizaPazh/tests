﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Models;
using DAL.Repositories;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class TaskServiceTests
    {
        readonly IMapper _mapper;
        readonly IUserRepository _userRepository;
        readonly List<ProjectTask> tasks;

        readonly ProjectTask task1 = new ProjectTask
        {
            Id = 32,
            Name = "Necessitatibus beatae animi unde velit nisi.",
            Description = "Dolorem ut eligendi blanditiis est labore. Quaerat voluptas reiciendis provident. Quae aperiam neque nemo omnis. Dicta suscipit possimus. Qui voluptatem tenetur. Est sequi corrupti.",
            CreatedAt = new DateTime(2020, 06, 01, 13, 59, 44, 16),
            State = TaskState.Started,
            ProjectId = 6,
            PerformerId = 1
        };

        readonly ProjectTask task2 = new ProjectTask
        {
            Id = 177,
            Name = "Necessitatibus beatae animi unde velit nisi................",
            Description = "Dolorem ut eligendi blanditiis est labore. Quaerat voluptas reiciendis provident. Quae aperiam neque nemo omnis. Dicta suscipit possimus. Qui voluptatem tenetur. Est sequi corrupti.",
            CreatedAt = new DateTime(2020, 06, 01, 13, 59, 44, 16),
            FinishedAt = new DateTime(2020, 07, 01, 13, 59, 44, 16),
            State = TaskState.Finished,
            ProjectId = 26,
            PerformerId = 1
        };
        public TaskServiceTests()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });
            _mapper = new Mapper(config);
            _userRepository = A.Fake<IUserRepository>();

            tasks = new List<ProjectTask> { task1, task2};
        }

        [Fact]
        public void TasksByUser_WhenUserWithId1_ThenReturn1Task()
        {
            var userId = task1.PerformerId;
            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _taskRepository.GetAll())
                .Returns(tasks);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            var result = service.TasksByUser(userId);

            Assert.Single(result);
            Assert.Equal(task1.Id, result.First().Id);
            Assert.Equal(userId, result.First().PerformerId);
            Assert.True(result.First().Name.Length < 45);
        }
        [Fact]
        public void FinishedTasksByUser_WhenTwoUsersWithOneFinishedTask_ThenReturnSecondTask()
        {
            var userId = task1.PerformerId;

            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _taskRepository.GetAll())
                .Returns(tasks);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            var result = service.FinishedTasksByUser(userId);

            Assert.Single(result);
            Assert.Equal(task2.Id, result.First().Item1);
            Assert.Equal(task2.Name, result.First().Item2);

        }
        [Fact]
        public void UnFinishedTasksByUser_WhenTwoUsersWithOneFinishedTask_ThenReturn1Task()
        {
            var userId = task1.PerformerId;

            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _taskRepository.GetAll())
                .Returns(tasks);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            var result = service.UnFinishedTasksByUser(userId);

            Assert.Single(result);
            Assert.Equal(task1.Id, result.FirstOrDefault().Id);
            Assert.Equal(userId, result.FirstOrDefault().PerformerId);
            Assert.NotEqual(TaskState.Finished, result.FirstOrDefault().State);
        }

        [Fact]
        public void UnFinishedTasksByUser_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            int userId = 1000;
            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _userRepository.GetById(userId))
               .Returns(null);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

           
            Assert.Throws<NotFoundException>(() => service.UnFinishedTasksByUser(userId));

        }

        [Fact]
        public void UpdateTask_WhenChangeStatusToFinished_ThenUpdateIsHappened()
        {
            var updatedTask = new TaskDTO
            {
                Id = task1.Id,
                State = TaskState.Finished
            };

            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _taskRepository.GetById(task1.Id))
                .Returns(task1);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            service.UpdateTask(updatedTask);

            A.CallTo(() => _taskRepository.Update(A<ProjectTask>.That
                                          .Matches(t=>t.Id == updatedTask.Id && t.State == updatedTask.State))).MustHaveHappenedOnceExactly();

        }

        [Fact]
        public void UpdateTask_WhenTaskNotExists_ExceptionThrown()
        {
            var notExistedId = 1000;

            var updatedTask = new TaskDTO
            {
                Id = notExistedId,
                State = TaskState.Finished,
            };

            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _taskRepository.GetById(notExistedId))
                .Returns(null);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            Assert.Throws<NotFoundException>(() => service.UpdateTask(updatedTask));
        }

        [Fact]
        public void FinishedTasksByUser_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            int userId = 1000;
            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _userRepository.GetById(userId))
               .Returns(null);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            Assert.Throws<NotFoundException>(() => service.FinishedTasksByUser(userId));
        }

        [Fact]
        public void TasksByUser_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            int userId = 1000;
            var _taskRepository = A.Fake<IProjectTaskRepository>();
            A.CallTo(() => _userRepository.GetById(userId))
               .Returns(null);

            var service = new TaskService(_taskRepository, _mapper, _userRepository);

            Assert.Throws<NotFoundException>(() => service.TasksByUser(userId));
        }
    }
}
