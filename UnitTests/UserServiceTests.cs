﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Models;
using DAL.Repositories;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class UserServiceTests
    {
        readonly IMapper _mapper;
        readonly List<User> users;
        readonly User user1 = new User
        {
            Id = 1,
            FirstName = "Retha",
            LastName = "Will",
            Email = "Retha67@yahoo.com",
            Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
            TeamId = 2,
            ProjectTasks = new List<ProjectTask>()
            {
                new ProjectTask
                {
                    Id = 115,
                    Name = "Similique possimus ipsam aliquid maiores laborum optio.",
                    Description = "Nihil sint qui illum et libero. Dolorem illum dolorem optio ipsam. Ea odit quis aut et autem non vel iusto.",
                    CreatedAt = new DateTime(2020,06,01,13,59,44,16),
                    FinishedAt = new DateTime(2020,10,16,14,22,44,65),
                    State = TaskState.Canceled,
                    ProjectId = 24,
                    PerformerId = 3
                },
                new ProjectTask
                {
                    Id = 114,
                    Name = "Similique possimus ipsam aliquid maiores laborum optio....................",
                    Description = "Nihil sint qui illum et libero. Dolorem illum dolorem optio ipsam. Ea odit quis aut et autem non vel iusto.",
                    CreatedAt = new DateTime(2020,07,01,13,59,44,16),
                    FinishedAt = new DateTime(2020,10,16,14,22,44,65),
                    State = TaskState.Canceled,
                    ProjectId = 24,
                    PerformerId = 3
                }
            }
        };

        readonly User user2 = new User
        {
            Id = 8,
            FirstName = "Atha",
            LastName = "Will",
            Email = "Retha67@yahoo.com",
            Birthday = new DateTime(2016, 07, 01, 13, 59, 44, 16),
            RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
            TeamId = 2,
            ProjectTasks = new List<ProjectTask>()
            {
                new ProjectTask
                {
                    Id = 116,
                    Name = "Similique possimus ipsam aliquid maiores laborum optio.",
                    Description = "Nihil sint qui illum et libero. Dolorem illum dolorem optio ipsam. Ea odit quis aut et autem non vel iusto.",
                    CreatedAt = new DateTime(2020,07,01,13,59,44,16),
                    FinishedAt = new DateTime(2020,10,16,14,22,44,65),
                    State = TaskState.Canceled,
                    ProjectId = 24,
                    PerformerId = 3
                }
            }
        };

        public UserServiceTests()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });

            _mapper = new Mapper(config);

            users = new List<User> { user1, user2 };
        }

        [Fact]
        public void UsersByABCWithTasksSortedByName_WhenTwoUsers_ThenReturnFirstSecondUser()
        {
            var _userRepository = A.Fake<IUserRepository>();
            var _projectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => _userRepository.GetAll(A<Expression<Func<User, object>>>.Ignored))
                .Returns(users);

            var service = new UserService(_userRepository, _projectRepository, _mapper);

            var result = service.UsersByABCWithTasksSortedByName();

            Assert.Equal(users.Count, result.Count());
            Assert.Equal(user2.Id, result.First().Id);
            Assert.Equal(user1.Id, result.Last().Id);
        }

        [Fact]
        public void GetUsersCharacteristics_WhenUserWithId1_ThenReturnUserWithId1()
        {
            int userId = user1.Id;
            int expectedNotCompletedOrCanceledTasksCount = 2;
            int expectedLongestTaskId = 115;


            var _userRepository = A.Fake<IUserRepository>();
            var _projectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => _userRepository.GetAll(A<Expression<Func<User, object>>>.Ignored))
                .Returns(users);

            var service = new UserService(_userRepository, _projectRepository, _mapper);

            var result = service.GetUsersCharacteristics(userId);

            Assert.Equal(userId, result.User.Id);
            Assert.Equal(expectedNotCompletedOrCanceledTasksCount, result.NotCompletedOrCanceledTasks);
            Assert.Equal(expectedLongestTaskId, result.LongestTask.Id);

        }
        [Fact]
        public void GetUsersCharacteristics_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            var _userRepository = A.Fake<IUserRepository>();
            var _projectRepository = A.Fake<IProjectRepository>();
            int userId = 1000;
            A.CallTo(() => _userRepository.GetById(userId))
               .Returns(null);
            var service = new UserService(_userRepository, _projectRepository, _mapper);


            Assert.Throws<NotFoundException>(() => service.GetUsersCharacteristics(1000));

        }

       [Fact]
        public void CreateUser_WhenCreate1User_ThenCreateIsHappened()
        {
            var user = new UserDTO
            {
                Id = 25,
                FirstName = "Retha",
                LastName = "Will",
                Email = "Retha67@yahoo.com",
                Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
                RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                TeamId = 2,
            };

            var _projectRepository = A.Fake<IProjectRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            var service = new UserService(_userRepository, _projectRepository, _mapper);

            service.CreateUser(user);

            A.CallTo(() => _userRepository.Create(A<User>.That.Matches(t => t.Id == user.Id))).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void CreateUser_WhenUserIsNull_ThenExceptionThrown()
        {
            UserDTO user = null;

            var _projectRepository = A.Fake<IProjectRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            var service = new UserService(_userRepository, _projectRepository, _mapper);

            Assert.Throws<ArgumentNullException>(() => service.CreateUser(user));
        }

    }
}
