﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public User Performer { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
    public enum TaskState
    {
        Created = 0,
        Started = 1,
        Finished = 2,
        Canceled = 3
    }
}
