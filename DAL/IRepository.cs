﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAL
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] properties);
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
        T GetById(int id);
    }
}
