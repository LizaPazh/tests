﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Context
{
    public static class ModelBuilderExtensions
    {
        static readonly DataSeeder dataSeeder = new DataSeeder();
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(p => p.Id).IsRequired();
            modelBuilder.Entity<Project>().Property(p => p.Name).HasMaxLength(100);
            modelBuilder.Entity<Project>().Property(p => p.Description).HasMaxLength(1000);
            modelBuilder.Entity<Project>().Property(p => p.CreatedAt).IsRequired();
            modelBuilder.Entity<Project>().Property(p => p.Deadline).IsRequired();

            modelBuilder.Entity<ProjectTask>().Property(p => p.Id).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(p => p.Name).HasMaxLength(100);
            modelBuilder.Entity<ProjectTask>().Property(p => p.Description).HasMaxLength(1000);
            modelBuilder.Entity<ProjectTask>().Property(p => p.CreatedAt).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(p => p.FinishedAt).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(p => p.State).IsRequired();

            modelBuilder.Entity<Team>().Property(p => p.Id).IsRequired();
            modelBuilder.Entity<Team>().Property(p => p.Name).HasMaxLength(100);
            modelBuilder.Entity<Team>().Property(p => p.CreatedAt).IsRequired();

            modelBuilder.Entity<User>().Property(p => p.Id).IsRequired();
            modelBuilder.Entity<User>().Property(p => p.FirstName).HasMaxLength(50);
            modelBuilder.Entity<User>().Property(p => p.LastName).HasMaxLength(50);
            modelBuilder.Entity<User>().Property(p => p.Email).HasMaxLength(50);
            modelBuilder.Entity<User>().Property(p => p.Birthday).IsRequired();
            modelBuilder.Entity<User>().Property(p => p.RegisteredAt).IsRequired();

            modelBuilder.Entity<Project>()
                .HasMany(p => p.ProjectTasks)
                .WithOne(c => c.Project)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ProjectTask>()
                .HasOne(t => t.Performer)
                .WithMany(p => p.ProjectTasks)
                .HasForeignKey(r => r.PerformerId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ProjectTask>()
                .HasOne(t => t.Project)
                .WithMany(p => p.ProjectTasks)
                .HasForeignKey(r => r.ProjectId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Team>()
               .HasMany(t => t.Projects)
               .WithOne(p => p.Team)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Team>()
               .HasMany(t => t.Users)
               .WithOne(u => u.Team)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(u => u.TeamId)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
               .HasMany(u => u.ProjectTasks)
               .WithOne(t => t.Performer)
               .OnDelete(DeleteBehavior.NoAction);
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var projects = dataSeeder.LoadProjects();
            var tasks = dataSeeder.LoadTasks();
            var teams = dataSeeder.LoadTeams();
            var users = dataSeeder.LoadUsers();

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<ProjectTask>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
        }
    }
}
