﻿using BLL.DTOs;
using Newtonsoft.Json;
using Project_Structure;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private const string BaseUrl = "http://localhost:44392/api";
        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateTeam_ThenResponseWithCode200AndCorrespondedBody()
        {
            var team = new TeamDTO { Name = "Neque quia nam autem iure." };

            string json = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync($"{BaseUrl}/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            await _client.DeleteAsync($"{BaseUrl}/projects/" + team.Id);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(team.Id, createdTeam.Id);
            Assert.Equal(team.Name, createdTeam.Name);
        }

        [Fact]
        public async Task CreateTeam_WhenEmptyTeam_ThenResponseCode400()
        {
            
            string json = JsonConvert.SerializeObject(null); ;
            var httpResponse = await _client.PostAsync($"{BaseUrl}/teams", new StringContent(json, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
            
        }

    }
}
